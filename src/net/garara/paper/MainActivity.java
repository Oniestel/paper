package net.garara.paper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.view.Menu;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private final static String FILENAME = "notes.txt"; //Default file
	private static String filepath = "";
	private EditText myEdit;	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //Hide title bar
        setContentView(R.layout.activity_main);
        myEdit = (EditText)findViewById(R.id.editText1);
        //Get external storage directory (/mnt/sdcard by default) and set set the application folder.
        filepath = Environment.getExternalStorageDirectory().getPath() + "/Android/data/net.garara.paper/files";
        loadFile();
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
   
   @Override   
   public void onPause() { //Save state
	   super.onPause();
       saveFile();
   }  
   
/**
 * Performs steps to initialize the application during the first run (create directory and file). Or just load notes. 
 */
private void loadFile(){
	   
		try
		{
			
			File dir = new File(filepath);
			if(!dir.exists() || !dir.isDirectory()) {
			    dir.mkdirs(); //Create folders and subfolders, if not exist.
			}
			
			File notesFile = new File(filepath + "/" +FILENAME); //Get file by path and name.
			
			if (!notesFile.exists()) {
				notesFile.createNewFile(); //Create file, if not exist.
				FileOutputStream outStream = new FileOutputStream(notesFile);
		        OutputStreamWriter streamWriter = new OutputStreamWriter(outStream); 
		        streamWriter.write("Your notes will be here :)"); 
		        streamWriter.close();
		        outStream.close();

			} else {
			    
			    FileInputStream inStream = new FileInputStream(notesFile);
				if(inStream != null)
				{
					InputStreamReader streamReader = new InputStreamReader(inStream);
					BufferedReader reader = new BufferedReader(streamReader);
					String strLine;
					StringBuffer buffer = new StringBuffer();
					
					while((strLine = reader.readLine()) != null)
					{
						buffer.append(strLine + "\n"); //Read the contents of the file. Line by line.
					}
					
					inStream.close();
					streamReader.close();
					myEdit.setText(buffer.toString()); //Show in editText.
					
				}
			}
		
		}
		catch(Throwable t)
		{
			Toast.makeText(getApplicationContext(), 
					"Exception: " + t.toString(), Toast.LENGTH_LONG).show(); //Something went wrong. Show it!
		}	
	   
   }
   
/**
 *Save notes to file. 
 */
private void saveFile() 
   {
       try 
       { 
    	   File notesFile = new File(filepath + "/" +FILENAME); //Get file by path and name.
    	   FileOutputStream outStream = new FileOutputStream(notesFile);
           OutputStreamWriter streamWriter = new OutputStreamWriter(outStream); 
           streamWriter.write(myEdit.getText().toString()); //Writing an notes from editText.
           streamWriter.close(); 
           outStream.close();
       }
       catch (Throwable t) 
       {
           Toast.makeText(getApplicationContext(), 
               "Exception: " + t.toString(), Toast.LENGTH_LONG).show(); //Something went wrong too.
       }
   }   
   
}